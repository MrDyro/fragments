package de.volkmar.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by mac on 03.08.13.
 */
public class FragmentTwo extends Fragment {

    private TextView tvText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_two, null);
        tvText = (TextView) layout.findViewById(R.id.tvFragmentTwo);

        return layout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String strText = getArguments().getString(MainActivity.BUNDLE_NAME_KEY);

        if(!TextUtils.isEmpty(strText)) {
            tvText.setText(strText);
        }
    }
}
